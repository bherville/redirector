class ApiConstraints
  def initialize(options)
    @version = options[:version]
    @default = options[:default]
  end

  def matches?(req)
    @default || req.headers['Accept'].include?("application/vnd.dvddb.v#{@version}")
  end

  def self.versions
    api_dir = File.join(Rails.root, 'app', 'controllers', 'api')

    (Dir.glob(File.join(api_dir, "*#{File::Separator}")).map { |a| File.basename(a).to_sym }).sort
  end
end