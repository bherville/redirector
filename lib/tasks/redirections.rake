namespace :redirections do
  require 'csv'

  desc 'Import redirects from a CSV file.'
  task :import_csv, [:file] => [:environment] do |f, args|
    csv_file = args[:file]

    redirections = Array.new
    CSV.foreach(csv_file, :headers => true) do |row|
      row['redirection_type'] ||= :r302
      row['user_id'] ||= SYSTEM_USER.id
      if row['from_url'] =~ /\A#{URI::regexp(%w(http https))}\z/
        # Create the redirect using the specified protocol (HTTP or HTTPS)
        redirections << Redirection.create!(row.to_hash)
      else
        # Create both HTTP and HTTPS redirects
        org_from_url = row['from_url']

        # Create HTTP Redirect
        row['from_url'] = "http://#{org_from_url}"
        redirections << Redirection.create(row.to_hash)

        # Create HTTPS Redirect
        row['from_url'] = "https://#{org_from_url}"
        redirections << Redirection.create(row.to_hash)
      end
    end

    puts "#{redirections.count} redirection(s) created"
  end

  desc 'Convert a CSV of redirections into a web server configuration.'
  task :csv_to_config, [:csv_file, :web_server, :listen, :listen_http, :listen_https] => [:environment] do |f, args|
    csv_file = args[:csv_file]
    web_server = args[:web_server].downcase.strip

    redirections = Array.new
    CSV.foreach(csv_file, :headers => true) do |row|
      row['redirection_type'] ||= :r302
      row['user_id'] ||= SYSTEM_USER.id
      if row['from_url'] =~ /\A#{URI::regexp(%w(http https))}\z/
        # Create the redirect using the specified protocol (HTTP or HTTPS)
        redirections << Redirection.new(row.to_hash)
      else
        # Create both HTTP and HTTPS redirects
        org_from_url = row['from_url']

        # Create HTTP Redirect
        row['from_url'] = "http://#{org_from_url}"
        redirections << Redirection.new(row.to_hash)

        # Create HTTPS Redirect
        row['from_url'] = "https://#{org_from_url}"
        redirections << Redirection.new(row.to_hash)
      end
    end

    redirections.each do |redirection|
      include Rails.application.routes.url_helpers
      include ActionView::Helpers::TagHelper

      av = ActionView::Base.new(Rails.root.join('app', 'views'))
      puts "#{av.render("redirections/web_servers/#{web_server}.text.erb", :redirection => redirection, :listen => args[:listen], :listen_http => args[:listen_http], :listen_https => args[:listen_https])}\n\n"
    end
  end
end
