class ChangeToQueryToRedirectToQuery < ActiveRecord::Migration
  def change
    rename_column :redirections, :to_query, :redirect_to_query
  end
end
