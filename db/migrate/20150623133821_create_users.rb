class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :lname
      t.string :fname
      t.string :time_zone
      t.integer :roles_mask
      t.string :provider
      t.integer :uid

      t.timestamps null: false
    end
  end
end
