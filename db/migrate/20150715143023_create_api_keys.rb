class CreateAPIKeys < ActiveRecord::Migration
  def change
    create_table :api_keys do |t|
      t.string :key
      t.string :application_name
      t.string :application_url
      t.text :description
      t.integer :user_id

      t.timestamps null: false
    end

    add_index :api_keys, :key, :unique => true
  end
end