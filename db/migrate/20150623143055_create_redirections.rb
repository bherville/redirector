class CreateRedirections < ActiveRecord::Migration
  def change
    create_table :redirections do |t|
      t.string :from_url, null: false
      t.string :to_url, null: false
      t.string :from_path, default: nil
      t.string :from_query, default: nil
      t.string :to_path, default: nil
      t.string :to_query, default: nil
      t.string :redirection_type, null: false
      t.integer :user_id, null: false
      t.boolean :ignore_user_path, default: true
      t.boolean :ignore_user_query, default: true

      t.timestamps null: false
    end
  end
end
