api_key="44cccd11ba5a5e8ad6afacee9de45fc6"

curl --header "Authorization: Token token=${api_key}" --data "redirection[from_url]=http://test&redirection[to_url]=http://test2&redirection[redirection_type]=r301" http://localhost:3000/api/redirections

redirection_id=73
curl -X PUT --header "Authorization: Token token=${api_key}" --data "redirection[redirection_type]=r302" http://localhost:3000/api/redirections/${redirection_id}


# Delete Resources
curl -X DELETE --header "Authorization: Token token=${api_key}" http://localhost:3000/api/redirections/${redirection_id}
