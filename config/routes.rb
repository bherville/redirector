Rails.application.routes.draw do
  resources :api_keys
  authenticated :user do
    root :to => 'redirections#index', :as => :authenticated_root
  end

  root :to => redirect('/users/sign_in')

  devise_for :users, :controllers => { :omniauth_callbacks => 'users/omniauth_callbacks' }

  resource :users, only: [:show] do
    resources :api_keys
  end

  resources :redirections do
    collection do
      get :redirect
    end
  end

  namespace :admin do
    get '/' => 'admin#index'
    resources :users
  end

  namespace :api, :defaults => {:format => :html} do
    root :to => 'api#index'
    resources :api, :controller => :api, only: [:index, :show]

    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true), :defaults => {:format => :json} do
      resources :redirections
    end

    match '*path', to: 'api_base#catch_404', via: :all, :format => :json
  end
end
