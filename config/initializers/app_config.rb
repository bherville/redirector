require 'yaml'

def validate_config(config)
  required_settings = {
  }

  required_settings.each do |setting, attributes|
    if config[setting.to_s].nil?
      raise "Missing setting #{setting} in config/app_config.yml for environment #{Rails.env}!"
    else
      attributes.each do |attribute|
        if config[setting.to_s][attribute].nil?
          raise "Missing setting #{setting} sub-setting #{attribute} in config/app_config.yml for environment #{Rails.env}!"
        end
      end
    end
  end
end

config = YAML.load_file("#{Rails.root}/config/app_config.yml")
if config.nil? || !config
  raise "Missing settings in config/app_config.yml for environment #{Rails.env}!"
else
  config = config[Rails.env]
  validate_config(config)
  APP_CONFIG = config
end