if ActiveRecord::Base.connection.table_exists?('users')
        SYSTEM_USER = User.create_with(
            :fname          => 'System',
            :lname          => 'User',
            :password       => rand(36**12).to_s(36),
            :confirmed_at   => Time.now
        ).find_or_create_by!(:email => 'system_user@redirect.example.org')
end