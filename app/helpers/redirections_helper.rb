module RedirectionsHelper
  def redirection_types_collection
    redirection_types = Array.new

    types = Redirection::REDIRECTION_TYPES.select { |k,v| k != :default }

    types.each do |k,v|
      redirection_types << {
          :key => k,
          :description => "#{v[:status_code]} #{v[:description]}"
      }
    end

    redirection_types
  end

  def redirection_type_to_s(redirection_type)
    type = Redirection::REDIRECTION_TYPES[redirection_type.to_sym]

    "#{type[:status_code]} #{type[:description]}"
  end
end
