class User < ActiveRecord::Base
  audited

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :lockable, :timeoutable

  devise :omniauthable, :omniauth_providers => [:auth_man]

  include RoleModel
  roles_attribute :roles_mask
  roles :admin, :redirect_creator, :user, :api_read_only, :api_read_write

  include Gravtastic
  gravtastic :secure => (AppConfig.get(%w(user_profiles secure), APP_CONFIG) ? AppConfig.get(%w(user_profiles secure), APP_CONFIG) : false)

  has_many :redirections
  has_many :api_keys, :dependent => :destroy

  before_create :set_default_roles

  def self.from_omniauth(auth)
    user_return = where(provider: auth.provider, uid: auth.uid).first

    unless user_return
      user = User.new(
          :email        => auth.info.email,
          :password     => Devise.friendly_token[0,20],
          :fname        => auth.info.fname,
          :lname        => auth.info.lname,
          :time_zone    => auth.info.time_zone,
      )

      user.uid      = auth.uid
      user.provider = auth.provider

      user.skip_confirmation!
      user.save

      user_return = user
    end

    user_return.sync_with_omniauth(auth)

    user_return
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session['devise.auth_man_data'] && session['devise.auth_man_data']['extra']['raw_info']
        user.email      = data['email'] if user.email.blank?
        user.fname      = data['fname'] if user.fname.blank?
        user.lname      = data['lname'] if user.lname.blank?
        user.time_zone  = data['time_zone'] if user.time_zone.blank?
      end
    end
  end

  def sync_with_omniauth(auth)
    unless auth.info.fname == self.fname && auth.info.lname == self.lname && auth.info.time_zone == self.time_zone
      self.fname      = auth.info.fname
      self.lname      = auth.info.lname
      self.time_zone  = auth.info.time_zone

      self.save
    end
  end

  def full_name
    "#{self.fname} #{self.lname}"
  end

  def full_name_comma
    "#{self.lname}, #{self.fname}"
  end

  private
  def set_default_roles
    self.roles << :user
    self.roles << :api_read_only
  end
end
