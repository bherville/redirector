class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new

    if user.has_role?(:admin)
      can :manage, :all
    elsif user.has_role?(:redirect_creator)
      can :manage, Redirection
    elsif user.has_role?(:user)
      can :read, [User, Redirection]
    else
      can :redirect
    end
  end
end
