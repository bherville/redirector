class Redirection < ActiveRecord::Base
    require 'csv'

    audited

    validates :from_url, :presence => true, :url => true
    validates :to_url, :presence => true, :url => true
    validates :redirection_type, :presence => true

    validates_uniqueness_of :to_url, :scope => [:from_url, :from_path, :from_query]

    validate :valid_redirection_type

    belongs_to :user

    before_save :strip
    before_save :nilify
    after_save :clear_cache

    REDIRECTION_TYPES = {
        :r300  => {
            :status_code  => 300,
            :description  => 'multiple choices'
        },
        :r301  => {
            :status_code  => 301,
            :description  => 'moved permanently'
        },
        :r302  => {
            :status_code  => 302,
            :description  => 'found'
        },
        :r303  => {
            :status_code  => 303,
            :description  => 'see other'
        },
        :r307  => {
            :status_code  => 307,
            :description  => 'temporary redirect'
        },
        :default  => :r301
    }

    CACHE_TAG = 'redirect_'

    serialize :redirection_type

    def build_url(options = {})
        uri = URI.parse(self.to_url)

        path = redirect_path(options[:to_path])
        query = redirect_query(options[:redirect_to_query])

        if uri.is_a?(URI::HTTPS)
            if Redirection::empty_string_to_nil(query)
                url = URI::HTTPS.build(:host => uri.host, :path => path, :query => "#{query}")
            else
                url = URI::HTTPS.build(:host => uri.host, :path => path)
            end
        elsif uri.is_a?(URI::HTTP)
            if Redirection::empty_string_to_nil(query)
                url = URI::HTTP.build(:host => uri.host, :path => path, :query => "#{query}")
            else
                url = URI::HTTP.build(:host => uri.host, :path => path)
            end
        else
            url = nil
        end

      url.to_s
    end

    def self.find_by_relevance(from_url, from_path = nil, from_query = nil)
      from_path = empty_string_to_nil(from_path).nil? ? 'from_path IS NULL' : "from_path = '#{from_path}'"
      from_query = empty_string_to_nil(from_query).nil? ? 'from_query IS NULL' : "from_query = '#{from_query}'"

      where_statements = Array.new

      where_statements << "from_url = '#{from_url}' AND #{from_path} AND #{from_query}" if from_path && from_query
      where_statements << "from_url = '#{from_url}' AND #{from_path} AND from_query IS NULL" if from_path
      where_statements << "from_url = '#{from_url}' AND from_path IS NULL AND #{from_query}" if from_query
      where_statements << "from_url = '#{from_url}' AND from_path IS NULL AND from_query IS NULL"


      where_statement_string = "(#{where_statements[0]})"
      where_statements.each_with_index do |where_statement, index|
        next if index == 0

        where_statement_string = "#{where_statement_string} OR (#{where_statement})"
      end


      where(where_statement_string)
    end

    def redirection_type_hash
      REDIRECTION_TYPES[self.redirection_type.to_sym]
    end

    def cache_tag(from_path = self.from_path)
      "#{CACHE_TAG}#{self.from_url}_#{from_path}_#{self.from_query}"
    end

    def self.cache_tag(params)
      if AppConfig.get(%w(redirections replace_from_path_empty), APP_CONFIG)
        if (params[:path].is_a?(String) && params[:path].empty?) || params[:path].nil?
          tmp_from_path = AppConfig.get(%w(redirections replace_from_path_empty), APP_CONFIG)
        else
          tmp_from_path = params[:path]
        end
      else
        tmp_from_path = ((params[:path].is_a?(String) && params[:path].empty?) || params[:path].nil?) ? nil : params[:path]
      end

      "#{CACHE_TAG}#{params[:from_url]}_#{tmp_from_path}_#{params[:query]}"
    end

    def write_to_cache(from_path = self.from_path)
      Rails.cache.write(self.cache_tag(from_path), self)
    end

    def self.to_csv(options = {})
      columns = {
          :id                 => 'ID',
          :from_url           => 'From URL',
          :from_path          => 'From Path',
          :from_query         => 'From Query',
          :to_url             => 'To URL',
          :to_path            => 'To Path',
          :redirect_to_query  => 'To Query',
          :redirection_type   => 'Redirection Type',
          :created_at         => 'Created At',
          :updated_at         => 'Updated At'
      }

      CSV.generate(options) do |csv|
        csv << columns.keys.map { |k| columns[k] }
        all.each do |redirection|
          csv << redirection.attributes.values_at(*columns.keys.map { |k| k.to_s })
        end
      end
    end

    private
    def valid_redirection_type

        errors.add(:redirection_type, 'is not valid') unless(self.redirection_type && REDIRECTION_TYPES.has_key?(self.redirection_type.to_sym))
    end

    def strip
      self.from_url             = self.from_url.strip if self.from_url.is_a?(String)
      self.from_path            = self.from_path.strip if self.from_path.is_a?(String)
      self.from_query           = self.from_query.strip if self.from_query.is_a?(String)

      self.to_url               = self.to_url.strip if self.to_url.is_a?(String)
      self.to_path              = self.to_path.strip if self.to_path.is_a?(String)
      self.redirect_to_query    = self.redirect_to_query.strip if self.redirect_to_query.is_a?(String)
    end

    def nilify
      self.from_url           = nil if(self.from_url.is_a?(String) && self.from_url.empty?)
      self.from_path          = AppConfig.get(%w(redirections replace_from_path_empty), APP_CONFIG) if((self.from_path.is_a?(String) && self.from_path.empty?) || self.from_path.nil?)
      self.from_query         = nil if(self.from_query.is_a?(String) && self.from_query.empty?)

      self.to_url             = nil if(self.to_url.is_a?(String) && self.to_url.empty?)
      self.to_path            = AppConfig.get(%w(redirections replace_to_path_empty), APP_CONFIG) if((self.to_path.is_a?(String) && self.to_path.empty?) || self.to_path.nil?)
      self.redirect_to_query  = nil if(self.redirect_to_query.is_a?(String) && self.redirect_to_query.empty?)
    end

    def self.empty_string_to_nil(string)
        if string.is_a?(String) && string.empty?
            nil
        else
            string
        end
    end

  def redirect_path(user_path = nil)
      if self.ignore_user_path?
          # Ignore the passed in user_path
          Redirection::empty_string_to_nil(self.to_path)
      else
        if user_path
            # Passed in user_path is not nil
            Redirection::empty_string_to_nil(user_path)
        else
            # Passed in user_path is nil
            Redirection::empty_string_to_nil(self.to_path)
        end
      end
  end

  def redirect_query(user_query = nil)
      if self.ignore_user_query?
          # Ignore the passed in user_query
          Redirection::empty_string_to_nil(self.redirect_to_query)
      else
          if user_query
              # Passed in user_query is not nil
              Redirection::empty_string_to_nil(user_query)
          else
              # Passed in user_query is nil
              Redirection::empty_string_to_nil(self.redirect_to_query)
          end
      end
  end

  def clear_cache
    if AppConfig.get(%w(redirections cache_redirects), APP_CONFIG)
      Rails.cache.delete(self.cache_tag) if AppConfig.get(%w(redirections cache_redirects_clear_on_save), APP_CONFIG)

      self.write_to_cache if AppConfig.get(%w(redirections cache_redirects_on_save), APP_CONFIG)
    end
  end
end
