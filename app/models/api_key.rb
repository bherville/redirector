class APIKey < ActiveRecord::Base
  audited

  belongs_to :user


  validates :application_name, :presence => true

  validates_uniqueness_of :application_name, :scope => [:user_id]


  before_create :generate_key

  private
  def generate_key
    self.key = SecureRandom.hex(AppConfig.get(%w(api api_key_hex_length), APP_CONFIG))
  end
end