json.partial! "api/#{self.controller_path.split('/').second}/redirections/redirection", redirection: @redirection
json.url api_redirection_url(@redirection, format: :json)