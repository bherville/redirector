class APIKeysController < ApplicationController
  load_and_authorize_resource
  before_action :set_api_key, only: [:show, :edit, :update, :destroy]
  before_action :set_user
  before_action :user_allowed?, only: [:show, :edit, :update, :destroy]

  # GET /api_keys
  def index
    @api_keys = @user.api_keys
  end

  # GET /api_keys/1
  def show
  end

  # GET /api_keys/new
  def new
    @api_key = @user.api_keys.new
  end

  # GET /api_keys/1/edit
  def edit
  end

  # POST /api_keys
  def create
    @api_key = @user.api_keys.new(api_key_params)

    respond_to do |format|
      if @api_key.save
        format.html { redirect_to users_api_key_path(@api_key), notice: 'API key was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /api_keys/1
  def update
    respond_to do |format|
      if @api_key.update(api_key_params)
        format.html { redirect_to users_api_key_path(@api_key), notice: 'API key was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /api_keys/1
  def destroy
    @api_key.destroy
    respond_to do |format|
      format.html { redirect_to users_api_keys_path, notice: 'API key was successfully destroyed.' }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_api_key
    @api_key = APIKey.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def api_key_params
    params.require(:api_key).permit(:key, :user_id, :application_name, :application_url, :description)
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = current_user
  end

  def user_allowed?
    raise CanCan::AccessDenied.new unless (@api_key.user == current_user || current_user.has_role?(:admin))
  end
end