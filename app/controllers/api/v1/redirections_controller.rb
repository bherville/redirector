class API::V1::RedirectionsController < API::APIBaseController
  load_and_authorize_resource
  before_action :set_redirection, only: [:show, :update, :destroy]

  # GET /redirections.json
  def index
    @redirections = Redirection.all
  end

  # GET /redirections/1.json
  def show
  end

  # POST /redirections.json
  def create
    @redirection = Redirection.new(redirection_params)
    @redirection.user = current_user

    respond_to do |format|
      if @redirection.save
        format.json { render :show, status: :created, location: redirection_path(@redirection) }
      else
        format.json { render json: @redirection.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /redirections/1.json
  def update
    respond_to do |format|
      if @redirection.update(redirection_params)
        format.json { render :show, status: :ok, location: redirection_path(@redirection) }
      else
        format.json { render json: @redirection.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /redirections/1.json
  def destroy
    @redirection.destroy

    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_redirection
    @redirection = Redirection.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def redirection_params
    params.require(:redirection).permit(:from_url, :to_url, :from_path, :from_query, :to_path, :redirect_to_query, :redirection_type, :ignore_user_path, :ignore_user_query).merge(:audit_comment => "api_key_id=#{current_api_key.id}")
  end
end