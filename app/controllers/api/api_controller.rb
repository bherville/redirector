class API::APIController < ApplicationController
  def index
    versions = ApiConstraints.versions

    api_versions = {
        :api => versions,
    }

    respond_to do |format|
      format.html
      format.json { render json: api_versions }
    end
  end
end