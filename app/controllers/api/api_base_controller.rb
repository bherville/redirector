class API::APIBaseController < ApplicationController
  rescue_from ::ActiveRecord::RecordNotFound, with: :record_not_found
  rescue_from ::ActiveRecord::RecordInvalid, with: :record_invalid
  rescue_from ::NameError, with: :error_occurred
  rescue_from ::ActionController::RoutingError, with: :error_occurred
  rescue_from ::PG::UniqueViolation, with: :record_duplicate
  rescue_from ::APIError::InvalidAPIKey, with: :invalid_api_key
  rescue_from ::APIError::MissingAPIKey, with: :missing_api_key

  rescue_from CanCan::AccessDenied do |exception|
    flash[:alert] = t('user.not_authorized')
    session[:user_return_to] = request.url

    respond_to do |format|
      format.json { render json: { :error => flash[:alert], :url => request.url, :status => 401 }, :status => 401 }
    end
  end

  def catch_404
    render json: APIError::Exceptions.page_not_found(Exception.new("The page you were looking for doesn't exist")).to_json, status: 404
  end

  protected
  def record_not_found(exception)
    render json: APIError::Exceptions.record_not_found(exception).to_json, status: 404
  end

  def record_invalid(exception)
    render json: APIError::Exceptions.record_invalid(exception).to_json, status: 422
  end

  def record_duplicate(exception = nil, options = {})
    excpt = ActiveRecord::RecordNotUnique.new(options[:message], exception)
    render json: APIError::Exceptions.record_duplicate(excpt).to_json, status: 500
  end

  def error_occurred(exception)
    render json: {error: exception.message}.to_json, status: 500
  end


  def missing_api_key(exception)
    status = 401

    render json: {error: exception.message, status: status}.to_json, status: status
  end


  def invalid_api_key(exception)
    status = 403

    render json: {error: exception.message, status: status}.to_json, status: status
  end

  def current_api_key
    api_key_param = params[:api_key]

    unless api_key_param
      authenticate_or_request_with_http_token do |token, options|
        api_key_param = token
      end
    end

    raise APIError::MissingAPIKey.new(t('exceptions.api_keys.missing_api_key')) unless api_key_param

    api_key = APIKey.find_by_key(api_key_param)
    raise APIError::InvalidAPIKey.new(t('exceptions.api_keys.invalid_api_key')) unless api_key

    api_key
  end

  def current_user
    current_api_key.user
  end

  def current_ability
    @current_ability ||= APIAbility.new(current_user)
  end

  def request_http_token_authentication(realm = 'Application')
    self.headers['WWW-Authenticate'] = %(Token realm="#{realm.gsub(/"/, '')}")
  end
end