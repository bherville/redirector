class RedirectionsController < ApplicationController
  load_and_authorize_resource
  skip_authorize_resource :only => [:redirect]
  before_action :set_redirection, only: [:show, :edit, :update, :destroy]
  before_action :text_type, only: [:index, :show]
  before_filter :owner?, only: [:edit, :update, :destroy]

  # GET /redirections
  def index
    @redirections = Redirection.all

    respond_to do |format|
      format.html
      format.text
      format.csv { send_data @redirections.to_csv }
    end
  end

  # GET /redirections/1
  def show
  end

  # GET /redirections/new
  def new
    @redirection = Redirection.new
  end

  # GET /redirections/1/edit
  def edit
  end

  # POST /redirections
  def create
    @redirection = Redirection.new(redirection_params)
    @redirection.user = current_user

    respond_to do |format|
      if @redirection.save
        format.html { redirect_to @redirection, notice: 'Redirection was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /redirections/1
  def update
    respond_to do |format|
      if @redirection.update(redirection_params)
        format.html { redirect_to @redirection, notice: 'Redirection was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /redirections/1
  def destroy
    @redirection.destroy
    respond_to do |format|
      format.html { redirect_to redirections_url, notice: 'Redirection was successfully destroyed.' }
    end
  end

  def redirect
    cache_redirects = AppConfig.get(%w(redirections cache_redirects), APP_CONFIG)

    if cache_redirects
      @redirection = Rails.cache.fetch(Redirection.cache_tag(params), :expires_in => cache_ttl.seconds)

      unless @redirection
        @redirection = Redirection.find_by_relevance(params[:from_url], params[:path], params[:query]).first

        @redirection.write_to_cache(params[:path]) if @redirection
      end
    else
      @redirection = Redirection.find_by_relevance(params[:from_url], params[:path], params[:query]).first
    end

    if @redirection
      fresh_when(:etag => @redirection, :last_modified => @redirection.created_at.utc, :public => true) if cache_redirects

      options = {
          :path => params[:path],
          :query => params[:query]
      }

      redirect_to(@redirection.build_url(options), :status => @redirection.redirection_type_hash[:status_code])
    else
      not_found
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_redirection
    @redirection = Redirection.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def redirection_params
    params.require(:redirection).permit(:from_url, :to_url, :from_path, :from_query, :to_path, :redirect_to_query, :redirection_type, :ignore_user_path, :ignore_user_query)
  end

  def owner?
    @redirection.user == current_user || current_user.has_role?(:admin)
  end

  def text_type
    if request.format.symbol == :text
      case params[:text_type]
        when 'apache'
          @text_type = :apache
        else
          @text_type = :apache
      end
    end
  end

  def cache_ttl
    cache_ttl = AppConfig.get(%w(redirections cache_redirects_ttl), APP_CONFIG)

    cache_ttl.is_a?(Integer) ? cache_ttl.to_i : 86400
  end
end
