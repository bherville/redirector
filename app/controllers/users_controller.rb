class UsersController < ApplicationController
  load_and_authorize_resource
  before_filter :set_user

  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = current_user
    end
end
